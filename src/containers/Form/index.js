import { useCallback, useState } from "react";
import PropTypes from "prop-types";
import Field, { FIELD_TYPES } from "../../components/Field";
import Select from "../../components/Select";
import Button, { BUTTON_TYPES } from "../../components/Button";

const mockContactApi = () =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, 500);
  });

const Form = ({ onSuccess, onError }) => {
  const [sending, setSending] = useState(false);
  const [nom, setNom] = useState("");
  const [prenom, setPrenom] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [type, setType] = useState("");
  const [errorMessage, setErrorMessage] = useState(""); // Nouvelle variable d'état pour l'erreur

  const handleChange = (fieldName, value) => {
    switch (fieldName) {
      case "Nom":
        setNom(value);
        break;
      case "Prénom":
        setPrenom(value);
        break;
      case "Email":
        setEmail(value);
        break;
      case "Message":
        setMessage(value);
        break;
      case "Personel / Entreprise":
        setType(value);
        break;
      default:
        break;
    }
  };

  const sendContact = useCallback(
    async (evt) => {
      evt.preventDefault();

      // Vérifiez si tous les champs sont remplis
      if (
        nom.trim() === "" ||
        prenom.trim() === "" ||
        email.trim() === "" ||
        message.trim() === "" ||
        type.trim() === ""
      ) {
        setErrorMessage("Veuillez remplir tous les champs du formulaire.");
        return;
      }

      setSending(true);

      // On essaie d'appeler l'API de contact (mockée ici)
      try {
        await mockContactApi();
        setSending(false);
        onSuccess();
      } catch (err) {
        setSending(false);
        onError(err);
      }
    },
    [nom, prenom, email, message, type, onSuccess, onError]
  );

  return (
    <form onSubmit={sendContact}>
      <div className="row">
        <div className="col">
          <Field
            placeholder=""
            label="Nom"
            onChange={(value) => handleChange("Nom", value)}
          />
          <Field
            placeholder=""
            label="Prénom"
            onChange={(value) => handleChange("Prénom", value)}
          />
          <Select
            selection={["Personel", "Entreprise"]}
            onChange={(value) => handleChange("Personel / Entreprise", value)}
            label="Personel / Entreprise"
            type="large"
            titleEmpty
          />
          <Field
            placeholder="Email"
            label="Email"
            onChange={(value) => handleChange("Email", value)}
          />
          <Button type={BUTTON_TYPES.SUBMIT} disabled={sending}>
            {sending ? "En cours" : "Envoyer"}
          </Button>
          {errorMessage && (
  <p style={{ color: 'red', fontWeight: 'bold' }}>{errorMessage}</p>
)}
        </div>
        <div className="col">
          <Field
            placeholder="message"
            label="Message"
            type={FIELD_TYPES.TEXTAREA}
            onChange={(value) => handleChange("Message", value)}
          />
        </div>
      </div>

    </form>
  );
};

Form.propTypes = {
  onError: PropTypes.func,
  onSuccess: PropTypes.func,
};

Form.defaultProps = {
  onError: () => null,
  onSuccess: () => null,
};

export default Form;
